/*
 * MoSimPa ESP32 firmware
 * Copyright 2020
 * Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 * Casanova Alejandro <acasanova.mails@gmail.com>
 * Pouso Marcelo Alejandro <mapouso86@gmail.com>
 */

#define ARDUINOJSON_USE_LONG_LONG 1
#include <ArduinoJson.h>
#include <Wire.h>

#include "MqttClient.h"
#include "max32664ahub.h"

MAX32664AHub::MAX32664AHub(uint16_t rstn, uint16_t mfio, TwoWire & wirePort):
    bioHub(rstn, mfio, wirePort), spo2Sensor(),
    heartRateSensor()
{
}

/**
 * @returns
 *  - 0x0 for Application mode
 *  - 0x8 for Bootloader mode
 *  - 0xFF if something went wrong with the communication.
 * statusByte != 0 then the value can not be used.
 */
uint8_t MAX32664AHub::begin()
{
    uint8_t statusByte;
    uint8_t retval = bioHub.startApplicationMode(statusByte);

    if (statusByte)
        return 0xFF;

    /// \todo Read MAX32664A's firmware version and fail loudly if it does not matches what we expect.

    return retval;
}

uint8_t MAX32664AHub::configAlgorithm(const int32_t coefA,
                                    const int32_t coefB,
                                    const int32_t coefC)
{
    /// Do not send coefficients, use the ones in the IC for now.
    uint8_t retval = bioHub.setCalibrationCoefficients(coefA, coefB, coefC);
    if(retval != 0x0)
        return retval;

    return bioHub.configAlgorithm();
}

uint8_t MAX32664AHub::readHrSpO2(bioData * bData, const uint8_t size, uint8_t & numReportsRead, uint8_t &totalNumReports)
{
    return bioHub.readHrSpO2(bData, size, numReportsRead, totalNumReports);
}

uint8_t MAX32664AHub::readCoefficients(int32_t coefArr[3])
{
    return bioHub.readCoefficients(coefArr);
}

void MAX32664AHub::clear()
{
    spo2Sensor.clear();
    heartRateSensor.clear();
}

bool MAX32664AHub::load(SpO2Sensor::Data & spo2Data,
                        HeartRateSensor::Data & hrData)
{
    return spo2Sensor.load(spo2Data) && heartRateSensor.load(hrData);
}

bool MAX32664AHub::buildJson()
{
    return spo2Sensor.buildJson() && heartRateSensor.buildJson();
}

void MAX32664AHub::flush(MqttClient &mqttClient)
{
    spo2Sensor.flush(mqttClient);
    heartRateSensor.flush(mqttClient);
}

bool MAX32664AHub::loadAndFlush(SpO2Sensor::Data & spo2Data,
                                HeartRateSensor::Data & hrData,
                                MqttClient & mqttClient)
{
    load(spo2Data, hrData);
    flush(mqttClient);
}

StaticJsonDocument < AbstractSensor::DOC_JSON_SIZE > MAX32664AHub::getSpo2Json()
{
    return spo2Sensor.getJson();
}

StaticJsonDocument < AbstractSensor::DOC_JSON_SIZE > MAX32664AHub::getHeartRateJson()
{
    return heartRateSensor.getJson();
}
