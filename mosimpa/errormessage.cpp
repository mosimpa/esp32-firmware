#include <WString.h>

#define ARDUINOJSON_USE_LONG_LONG 1
#include <ArduinoJson.h>

#include "Configuration.h"
#include "errormessage.h"

ErrorMessage::ErrorMessage(const String file, const String func, int line, int retval)
{
    _file = file;
    _func = func;
    _line = line;
    _retval = retval;

    // Clean up the file path.
    int index = _file.lastIndexOf('/');
    _file.remove(0, index+1);
}

String ErrorMessage::json() const
{
    // The message will be at least 256 bytes long.
    StaticJsonDocument<256> jsonDoc;
    String msg;

    // Create an empty doc.
    jsonDoc.to<JsonObject>();

    jsonDoc["file"] = _file;
    jsonDoc["func"] = _func;
    jsonDoc["line"] = _line;
    jsonDoc["retval"] = _retval;
    jsonDoc["firm_ver"] = FIRMWARE_VERSION;

    serializeJson(jsonDoc, msg);

    return msg;
}
