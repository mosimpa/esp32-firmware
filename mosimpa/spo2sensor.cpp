#include <Arduino.h>
#include <time.h>
#include "MqttClient.h"

#include "spo2sensor.h"

SpO2Sensor::SpO2Sensor()
{
}

bool SpO2Sensor::load(Data &data)
{
    if (count == SPO2_SENSOR_BUFFER_SIZE) {
        return false;
    }
    buffer[indexWrite] = data;
    if (++indexWrite == SPO2_SENSOR_BUFFER_SIZE) {
        indexWrite = 0;
    }
    count++;
    return true;
}

// {
//  "spo2": [
//    { "time": 1212144844, "SpO2": 0 }
//  ]
// }
bool SpO2Sensor::buildJson()
{
    SpO2Sensor::Data data;
    if (count == 0) {
        return false;
    }
    // Generacion JSON
    jsonDocument.clear();
    while (count > 0) {
        JsonArray arrayName = jsonDocument.createNestedArray("spo2");
        read(&data);
        JsonObject arrayItemm = arrayName.createNestedObject();
        arrayItemm["time"] = data.time;
        arrayItemm["SpO2"] = data.spO2;
    }
    return true;
}

bool SpO2Sensor::loadAndFlush(Data &data, MqttClient & mqttClient)
{
    load(data);
    flush(mqttClient);
}

void SpO2Sensor::read(SpO2Sensor::Data * data)
{
    if (count == 0) {
        return;
    }
    *data = buffer[indexRead];
    if (++indexRead == SPO2_SENSOR_BUFFER_SIZE) {
        indexRead = 0;
    }
    count--;
}
