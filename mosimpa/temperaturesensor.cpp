#include <Arduino.h>
#include <time.h>
#include "MqttClient.h"

#include "temperaturesensor.h"

TemperatureSensor::TemperatureSensor(TwoWire & wirePort):temperatureSensor(wirePort)
{
}

void TemperatureSensor::begin()
{
    temperatureSensor.begin();
}

int16_t TemperatureSensor::getIntTemperature()
{
    return temperatureSensor.getIntTemperature();
}

float TemperatureSensor::getTemperature()
{
    return temperatureSensor.getTemperature();
}

bool TemperatureSensor::load(Data &data)
{
    if (count == TEMP_SENSOR_BUFFER_SIZE)
        return false;

    buffer[indexWrite] = data;

    if (++indexWrite == TEMP_SENSOR_BUFFER_SIZE)
        indexWrite = 0;

    count++;
    return true;
}

// {
//  "bodyT": [
//    { "time": 1212144844, "temp": 0 }
//  ]
// }
bool TemperatureSensor::buildJson()
{
    TemperatureSensor::Data data;
    if (count == 0)
        return false;

    // Generacion JSON
    jsonDocument.clear();
    while (count > 0)
    {
        JsonArray arrayName = jsonDocument.createNestedArray("bodyT");
        read(&data);
        JsonObject arrayItemm = arrayName.createNestedObject();
        arrayItemm["time"] = data.time;
        arrayItemm["temp"] = data.temp;
    }
    return true;
}

bool TemperatureSensor::loadAndFlush(TemperatureSensor::Data & data, MqttClient &mqttClient)
{
    load(data);
    flush(mqttClient);
}

void TemperatureSensor::read(TemperatureSensor::Data * data)
{
    if (count == 0) {
        return;
    }
    *data = buffer[indexRead];
    if (++indexRead == TEMP_SENSOR_BUFFER_SIZE) {
        indexRead = 0;
    }
    count--;
}
