/*
 * MoSimPa ESP32 firmware
 * Copyright 2020
 * Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 * Casanova Alejandro <acasanova.mails@gmail.com>
 * Pouso Marcelo Alejandro <mapouso86@gmail.com>
 */
#ifndef BLOODPRESSURESENSOR_H
#define BLOODPRESSURESENSOR_H

#define ARDUINOJSON_USE_LONG_LONG 1
#include <ArduinoJson.h>
#include "MqttClient.h"

#include "abstractsensor.h"

class BloodPressureSensor : public AbstractSensor
{
public:

    struct Data {
        time_t time;
        uint8_t sys;
        uint8_t dia;
    };

    BloodPressureSensor();

    bool load(Data & data);
    bool buildJson();
    bool loadAndFlush(Data & data, MqttClient & mqttClient);

protected:
    void read(Data * data);

    static const int BLOODP_SENSOR_BUFFER_SIZE = 10;
    Data buffer[BLOODP_SENSOR_BUFFER_SIZE];
};

#endif // BLOODPRESSURESENSOR_H
