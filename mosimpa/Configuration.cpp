/*
 * MoSimPa ESP32 firmware
 * Copyright 2020
 * Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 * Casanova Alejandro <acasanova.mails@gmail.com>
 * Pouso Marcelo Alejandro <mapouso86@gmail.com>
 */
#include "Configuration.h"
#include "printf.h"
#include <EEPROM.h>

ConfigurationClass::ConfigurationClass()
{
}

/**
 * @brief ConfigurationClass::begin
 * Initializes the non volatile memory to store configuration. Should be called
 * before using any other method.
 */
void ConfigurationClass::begin()
{
    parameters.flag = 0xFF;
    EEPROM.begin(100);
}

/**
 * @brief ConfigurationClass::isSetting
 * @return True if there are settings stored in NVRAM.
 */
bool ConfigurationClass::isSetting(void)
{
    Parameters tmp;

    EEPROM.get(0, tmp);
    if (tmp.flag == 0xA5) {
        parameters = tmp;
        return true;
    }

    return false;
}

/**
 * @brief ConfigurationClass::getSSID
 * @return The WiFi SSID to which the device should connect to.
 */
char *ConfigurationClass::getSSID(void)
{
    if (parameters.flag == 0xA5) {
        return &parameters.wifissid[0];
    }

    return NULL;
}

/**
 * @brief ConfigurationClass::getPass
 * @return The password ot be used with the WiFi connection.
 */
char *ConfigurationClass::getPass(void)
{
    if (parameters.flag == 0xA5) {
        return &parameters.wifipass[0];
    }

    return NULL;
}

int ConfigurationClass::getMqttPort(void)
{
    if (parameters.flag == 0xA5) {
        return parameters.mqttport;
    }

    return 0;
}

char *ConfigurationClass::getMqttHostname(void)
{
    if (parameters.flag == 0xA5) {
        return &parameters.mqtthostname[0];
    }

    return NULL;
}

void ConfigurationClass::setSSID(String ssid)
{
    ssid.toCharArray(parameters.wifissid, WIFI_SSID_SIZE);
}

void ConfigurationClass::setPass(String pass)
{
    pass.toCharArray(parameters.wifipass, WIFI_PASS_SIZE);
}

void ConfigurationClass::setMqttPort(int port)
{
    parameters.mqttport = port;
}

void ConfigurationClass::setMqttHostname(String hostname)
{
    hostname.toCharArray(parameters.mqtthostname,
                         MQTT_BROKER_HOSTNAME_SIZE);
}

/**
 * @brief ConfigurationClass::commit Stores the configuration in NVRAM.
 */
void ConfigurationClass::commit(void)
{
    parameters.flag = 0xA5;
    EEPROM.put(0, parameters);
    EEPROM.commit();

    printfn("Configuration commit: flag = 0x%02X", parameters.flag);
    printfn("Configuration commit: ssid = %s", parameters.wifissid);
    printfn("Configuration commit: pass = %s", parameters.wifipass);
    printfn("Configuration commit: mqttport = %u", parameters.mqttport);
    printfn("Configuration commit: mqtthostname = %s", parameters.mqtthostname);
}

ConfigurationClass Configuration;
