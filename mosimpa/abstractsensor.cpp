#include <Arduino.h>
#include <time.h>
#include "MqttClient.h"

#include "abstractsensor.h"

AbstractSensor::AbstractSensor()
{
    clear();
}

void AbstractSensor::clear()
{
    indexWrite = 0;
    indexRead = 0;
    count = 0;
}

/**
 * @brief AbstractSensor::flush
 * @param mqttClient MQTT client in use.
 * Flushes the data to a MQTT broker, topic "reads"
 */
void AbstractSensor::flush(MqttClient &mqttClient)
{
    String message;
    if (buildJson()) {
        serializeJson(jsonDocument, message);
        String topic("reads/" + mqttClient.getID());
        mqttClient.publish(topic.c_str(), message.c_str());
    }
}

/**
 * @brief AbstractSensor::getJson
 * @return Returns a static JSON document ready to be used.
 */
StaticJsonDocument<AbstractSensor::DOC_JSON_SIZE> AbstractSensor::getJson()
{
    return jsonDocument;
}
