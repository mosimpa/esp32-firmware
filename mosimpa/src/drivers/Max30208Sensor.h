/*
 * MoSimPa ESP32 firmware
 * Copyright 2020 
 * Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 * Casanova Alejandro <acasanova.mails@gmail.com>
 * Pouso Marcelo Alejandro <mapouso86@gmail.com>
 */ 

#ifndef MAX30208_SENSOR_H
#define MAX30208_SENSOR_H

#include <Wire.h>

#define MAX30208_ADDRESS                    0x50  // 8bit address converted to 7bit
// Registers
#define MAX30208_STATUS                     0x00  //  
#define MAX30208_INTERRUPT_ENABLE           0x01
#define MAX30208_FIFO_WRITE_POINTER         0x04
#define MAX30208_FIFO_READ_POINTER          0x05
#define MAX30208_FIFO_OVERFLOW_COUNTER      0x06
#define MAX30208_FIFO_DATA_COUNTER          0x07
#define MAX30208_FIFO_DATA                  0x08
#define MAX30208_FIFO_CONFIGURATION_1       0x09 
#define MAX30208_FIFO_CONFIGURATION_2       0x0A
#define MAX30208_SYSTEM_CONTROL             0x0C
#define MAX30208_ALARM_HIGH_MSB             0x10
#define MAX30208_ALARM_HIGH_LSB             0x11
#define MAX30208_ALARM_LOW_MSB              0x12
#define MAX30208_ALARM_LOW_LSB              0x13
#define MAX30208_SENSOR_SETUP               0x14
#define MAX30208_GPIO_SETUP                 0x20
#define MAX30208_GPIO_CONTROL               0x21
#define MAX30208_PART_ID_1                  0x31
#define MAX30208_PART_ID_2                  0x32
#define MAX30208_PART_ID_3                  0x33
#define MAX30208_PART_ID_4                  0x34
#define MAX30208_PART_ID_5                  0x35
#define MAX30208_PART_ID_6                  0x36
#define MAX30208_PART_IDENTIFIER            0xFF

class Max30208Sensor {
public:
  Max30208Sensor(TwoWire &wirePort = Wire);
  void begin(void);
  float getTemperature(void);
  int16_t getIntTemperature(void);

private:
  TwoWire *_i2cPort;

  static const int32_t MUL_FACTOR = 10;

  int16_t rawTemperature();

  void    I2CWriteByte(uint8_t address, uint8_t subAddress, uint8_t data);
  uint8_t I2CReadByte(uint8_t address, uint8_t subAddress);
  void    I2CReadBytes(uint8_t address, uint8_t subAddress, uint8_t * dest, uint8_t count);
};

#endif
