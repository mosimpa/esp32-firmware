/*
 * Copyright 2020 Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 * License: Apache 2.0
 *
 * This file is based in SParkFun's Bio Hub Library
 * https://github.com/sparkfun/SparkFun_Bio_Sensor_Hub_Library
 * and of course, datasheets.
 */
#ifndef MAX32664A_H
#define MAX32664A_H

#include <Wire.h>
#include <Arduino.h>

/*@{*/
struct bioData {
    uint16_t heartRate;		///< LSB = 0.1bpm
    uint16_t oxygen;		///< 0-1000 0.1% LSB = 0.1%
    uint8_t confidence;		///< 0-100% LSB = 1%
    uint8_t status;		///< 0: Success, 1: Not Ready, 2: Object Detected, 3: Finger Detected.
};
/*@}*/

class MAX32664A {
public:
    static const uint8_t MAXFAST_ARRAY_SIZE = 6;
    static const int MAX_REPORTS_NUMBER = I2C_BUFFER_LENGTH / MAXFAST_ARRAY_SIZE;

    MAX32664A(const uint8_t resetGpio, const uint8_t mfioGpio,
              TwoWire & twoWire, const uint8_t address = 0x55);

    uint8_t startApplicationMode(uint8_t & statusByte);

    uint8_t setCalibrationCoefficients(const int32_t coefA,
                                       const int32_t coefB,
                                       const int32_t coefC);
    uint8_t configAlgorithm();

    uint8_t readHrSpO2(bioData * bData, const uint8_t size, uint8_t & numReportsRead, uint8_t &totalNumReports);

    uint8_t readCoefficients(int32_t coefficients[3]);

private:
    enum class FamilyByte : uint8_t {
        SensorHubStatus = 0x00,
        ReadDevice = 0x02,
        OutputMode = 0x10,
        ReadOutputFifo = 0x12,
        EnableSensor = 0x44,
        AlgorithmConfig = 0x50,
        AlgorithmConfigRead = 0x51,
        EnableAlgorithm = 0x52,
    };

    enum class IndexByte : uint8_t {
        SensorHubStatus = 0x00,
        OperatingMode = 0x00,
        NumOfFifoSamples = 0x00,
        SetOutputFormat = 0x00,
        EnableAgcAlgo = 0x00,
        ReadFifoData = 0x01,
        SetThreshold = 0x01,
        AlgoCoefficients = 0x02,
        EnableWhrmAlgorithm = 0x02,
        EnableMax30101 = 0x03,
    };

    enum class AlgoAGCWriteByte : uint8_t {
        GainId = 0x00,
        StepSizeId = 0x01,
        SensitivityId = 0x02,
        NumSamplesId = 0x03,
        CoefficientsId = 0x0b,
    };

    enum class OutputMode : uint8_t {
        Pause = 0x00,
        Sensor = 0x01,
        Algorithm = 0x02,
        SensorAndAlgorithm = 0x03,
        SampleCounterAndSensor = 0x05,
        SampleCounterAndAlgorithm = 0x06,
        SampleCounterAndSensorAndAlgorithm = 0x07,
    };

    enum class Enable : uint8_t {
        No = 0x00,
        Yes = 0x01,
    };

    enum class AlgorithmMode : uint8_t {
        Disable = 0x00,
        One = 0x01,
        Two = 0x02,
    };

    enum class SsStatus : uint8_t {
        Success = 0x00,
        ErrCommand = 0x01,
        ErrUnavailable = 0x02,
        ErrDataFormat = 0x03,
        ErrInputValue = 0x04,
        ErrBtldrGeneral = 0x80,
        ErrBtldrChecksum = 0x81,
        ErrTryAgain = 0xFE,
        ErrUnknown = 0xFF,
    };

    static const int16_t DEFAULT_CMD_SLEEP_MS = 6 ;
    static const int16_t ENABLE_SENSOR_SLEEP_MS = 20;
    static const int16_t DUMP_REGISTER_SLEEP_MS = 100;

    static const uint8_t MAXFAST_EXTENDED_DATA = 5;
    static const int8_t  DEFAULT_RETRIES = 5;

    uint8_t readByte(const enum FamilyByte familyByte,
                     const enum IndexByte indexByte, uint8_t & statusByte);
    uint8_t readFillArray(const enum FamilyByte familyByte,
                          const enum IndexByte indexByte,
                          const uint8_t arraySize, uint8_t array[]);
    uint8_t writeByte(const enum FamilyByte familyByte,
                      const enum IndexByte indexByte, const uint8_t byte, const unsigned long delayMs);

    uint8_t writeCmd(const int8_t txLen, const int16_t sleepMs);
    uint8_t writeCmdWithData(uint8_t * cmdBytes, const uint8_t cmdBytesLen,
                             uint8_t * data, const uint8_t dataLen,
                             const int16_t delayMs);
    uint8_t readCmd(uint8_t * cmdBytes, const int cmdLen,
                    uint8_t * data, const int dataLen,
                    uint8_t * rxBuffer, const uint8_t rxLen, const int16_t delayMs);

    uint8_t readSensorHubStatus(uint8_t & statusByte);
    uint8_t numSamplesOutFifo(uint8_t & statusByte);
    uint8_t setOutputMode(const OutputMode mode);
    uint8_t setFifoThreshold(const uint8_t threshold);
    uint8_t agcAlgorithmControl(const Enable enable);
    uint8_t max30101Control(const Enable enable);
    uint8_t fastAlgorithmControl(const AlgorithmMode mode);

    uint8_t _resetGpio;
    uint8_t _mfioGpio;
    uint8_t _hubAddress;
    TwoWire *_twoWire;
    uint8_t _sampleRate;
    AlgorithmMode _algorithmMode;

    uint8_t _txBuffer[I2C_BUFFER_LENGTH];
};

#endif
