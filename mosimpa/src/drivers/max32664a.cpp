/*
 * Copyright 2020 Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 * License: Apache 2.0
 *
 * This file is based in SParkFun's Bio Hub Library
 * https://github.com/sparkfun/SparkFun_Bio_Sensor_Hub_Library
 * and of course, datasheets.
 */

#include <Arduino.h>
#include <Wire.h>

#include "max32664a.h"

MAX32664A::MAX32664A(const uint8_t resetGpio, const uint8_t mfioGpio,
                     TwoWire & twoWire, const uint8_t address)
{
    _resetGpio = resetGpio;
    _mfioGpio = mfioGpio;
    _hubAddress = address;
    _twoWire = &twoWire;
}

/**
 * @brief MAX32664A::startApplicationMode
 * @param statusByte Returns the status byte of the I²C read. The returned value
 * should be discarded if statusByte != 0.
 * @return When statusByte == 0 it returns the current mode.
 */
uint8_t MAX32664A::startApplicationMode(uint8_t & statusByte)
{
    pinMode(_mfioGpio, OUTPUT);
    pinMode(_resetGpio, OUTPUT);

    digitalWrite(_resetGpio, LOW);
    delay(10);
    digitalWrite(_mfioGpio, HIGH);
    delay(10);
    digitalWrite(_resetGpio, HIGH);

    // Last wait in order to get the I²C interface responding.
    delay(1000);

    // Now this pin becomes an interrupt...
    pinMode(_mfioGpio, INPUT);

    return readByte(FamilyByte::ReadDevice, IndexByte::OperatingMode,
                    statusByte);
}

/**
 * @brief MAX32664A::setCalibrationCoefficients
 * @param coefA Coefficient A
 * @param coefB Coefficient B
 * @param coefC Coefficient C
 * @return 0 if the call was succesful.
 *
 * The coefficients need to be in integer format, ie, in the form the MAX32664A
 * expects them. See Section 2 of UG7087, Rev 1.
 */
uint8_t MAX32664A::setCalibrationCoefficients(const int32_t coefA,
                                              const int32_t coefB,
                                              const int32_t coefC)
{
    uint8_t statusByte;
    uint8_t cmd[3] = { static_cast < uint8_t > (FamilyByte::AlgorithmConfig),
                       static_cast < uint8_t > (IndexByte::AlgoCoefficients),
                       static_cast < uint8_t > (AlgoAGCWriteByte::CoefficientsId)};

    uint8_t coefs[3*sizeof(int32_t)] = {
        static_cast<uint8_t>(coefA >> 24), static_cast<uint8_t>(coefA >> 16), static_cast<uint8_t>(coefA >> 8), static_cast<uint8_t>(coefA),
        static_cast<uint8_t>(coefB >> 24), static_cast<uint8_t>(coefB >> 16), static_cast<uint8_t>(coefB >> 8), static_cast<uint8_t>(coefB),
        static_cast<uint8_t>(coefC >> 24), static_cast<uint8_t>(coefC >> 16), static_cast<uint8_t>(coefC >> 8), static_cast<uint8_t>(coefC)
    };

    statusByte = writeCmdWithData(cmd, 3, coefs, 3*sizeof(int32_t), DEFAULT_CMD_SLEEP_MS);
    return statusByte;

}

uint8_t MAX32664A::configAlgorithm()
{
    uint8_t status;

    status = setOutputMode(OutputMode::Algorithm);
    if (status)
        return status;

    status = setFifoThreshold(15);
    if (status)
        return status;

    status = agcAlgorithmControl(Enable::Yes);
    if (status)
        return status;

    status = max30101Control(Enable::Yes);
    if (status)
        return status;

    status = fastAlgorithmControl(AlgorithmMode::One);
    if (status)
        return status;

    return 0;
}

/**
 * @brief MAX32664A::readHrSpO2 reads all the reports available in the FIFO.
 * @param bData Array of bioData.
 * @param size Number of bioData structs in bData.
 * @param numReportsRead Number of reports returned.
 * @param totalNumReports Total number of reports available in the FIFO before
 * reading it.
 * @return statusByte, adding:
 * - 0x70 if algorythm is not One.
 * - 0x71 if there was a communication error while reading the status.
 *
 * Be sure to check bioData[i].statusByte. If this value is != 0 then the
 * contents of the array are undefined.
 */
uint8_t MAX32664A::readHrSpO2(bioData * bData, const uint8_t size, uint8_t & numReportsRead, uint8_t & totalNumReports)
{
    uint8_t statusByte;
    uint8_t status;

    numReportsRead = 0;

    if (_algorithmMode != AlgorithmMode::One)
        return 0x70;

    status = readSensorHubStatus(statusByte);
    if (statusByte)
        return statusByte;

    /*
     * Sensor comm error, return our own value not defined according to the
     * datasheets, table Read status byte value.
     */
    if ((status & 1) == 1)
        return 0x71;

    if ((status & 8) != 8)
        return 0x0;

    totalNumReports = numSamplesOutFifo(statusByte);
    if (statusByte)
        return statusByte;

    numReportsRead = totalNumReports;

    // Check that we can handle enough reports.
    if(size < numReportsRead)
        numReportsRead = size;

    // Check that we have enough space in the ESP32 I²C FIFO.
    int32_t numReportBytes = MAXFAST_ARRAY_SIZE * numReportsRead;
    if(numReportBytes > I2C_BUFFER_LENGTH)
    {
        numReportsRead = I2C_BUFFER_LENGTH / MAXFAST_ARRAY_SIZE;
        numReportBytes = numReportsRead * MAXFAST_ARRAY_SIZE;
    }

    uint8_t reports[numReportBytes];

    statusByte = readFillArray(FamilyByte::ReadOutputFifo, IndexByte::ReadFifoData,
                               numReportBytes, reports);
    if (statusByte)
        return statusByte;

    for (int i = 0; i < numReportsRead; i++)
    {
        bData[i].heartRate = (uint16_t(reports[i * MAXFAST_ARRAY_SIZE]) << 8);
        bData[i].heartRate |= (reports[i * MAXFAST_ARRAY_SIZE + 1]);
        bData[i].confidence = reports[i * MAXFAST_ARRAY_SIZE + 2];
        bData[i].oxygen = uint16_t(reports[i * MAXFAST_ARRAY_SIZE + 3]) << 8;
        bData[i].oxygen |= reports[i * MAXFAST_ARRAY_SIZE + 4];
        bData[i].status = reports[i * MAXFAST_ARRAY_SIZE + 5];
    }

    return 0;
}

/**
 * @brief MAX32664A::readCoefficients
 * @param coefficients Array of 3 int32_t in which the coefficients will be written.
 * @return 0 is successful.
 *
 * The returned coefficients will be in integer format, ie, in the exact same
 * format the MAX32664A uses them.
 */
uint8_t MAX32664A::readCoefficients(int32_t coefficients[])
{
    _txBuffer[0] = static_cast<uint8_t>(FamilyByte::AlgorithmConfigRead);
    _txBuffer[1] = static_cast<uint8_t>(IndexByte::AlgoCoefficients);
    _txBuffer[2] = static_cast<uint8_t>(AlgoAGCWriteByte::CoefficientsId);

    uint8_t data[3*sizeof(int32_t)];

    uint8_t statusByte = readCmd(&_txBuffer[0], 3,
                                 static_cast<uint8_t*>(0), 0,
                                 data, 3*sizeof(int32_t), DUMP_REGISTER_SLEEP_MS);

    for(int i = 0; i < 3; i++)
    {
        coefficients[i] |= (data[4*i] << 24);
        coefficients[i] |= (data[4*i+1] << 16);
        coefficients[i] |= (data[4*i+2] << 8);
        coefficients[i] |= data[4*i+3];
    }

    return statusByte;
}

/**
 * @brief MAX32664A::readByte reads a byte.
 * @param statusByte 0 if the operation was successful, != 0 otherwise.
 * @return The required byte if statusByte != 0.
 */
uint8_t MAX32664A::readByte(const MAX32664A::FamilyByte familyByte,
                            const MAX32664A::IndexByte indexByte,
                            uint8_t & statusByte)
{
    uint8_t byte;

    _txBuffer[0] = static_cast<uint8_t>(familyByte);
    _txBuffer[1] = static_cast<uint8_t>(indexByte);

    statusByte = readCmd(&_txBuffer[0], 2,
                         static_cast<uint8_t*>(0), 0,
                         &byte, 1, DEFAULT_CMD_SLEEP_MS);

    return byte;
}

/**
 * @brief MAX32664A::readFillArray Reads an fills an array of data.
 * @param familyByte
 * @param indexByte
 * @param arraySize number of bytes to be read.
 * @param array Array in which to store the bytes read. It must be at least
 * arraySize large.
 * @return statusByte, 0 if successful.
 */
uint8_t MAX32664A::readFillArray(const MAX32664A::FamilyByte familyByte,
                                 const MAX32664A::IndexByte indexByte,
                                 const uint8_t arraySize, uint8_t array[])
{
    _txBuffer[0] = static_cast<uint8_t>(familyByte);
    _txBuffer[1] = static_cast<uint8_t>(indexByte);
    uint8_t retval = readCmd(&_txBuffer[0], 2,
                             static_cast<uint8_t*>(0), 0,
                             array, arraySize, DEFAULT_CMD_SLEEP_MS);
    return retval;
}

uint8_t MAX32664A::writeByte(const MAX32664A::FamilyByte familyByte,
                             const MAX32664A::IndexByte indexByte,
                             const uint8_t byte, const unsigned long delayMs)
{
    _txBuffer[0] = static_cast<uint8_t>(familyByte);
    _txBuffer[1] = static_cast<uint8_t>(indexByte);
    _txBuffer[2] = byte;

    return writeCmd(3, delayMs);
}

uint8_t MAX32664A::writeCmd(const int8_t txLen, const int16_t sleepMs)
{
    auto retries = DEFAULT_RETRIES;
    uint8_t retval = 0;
    uint8_t statusByte;

    if(txLen > I2C_BUFFER_LENGTH)
        return 0x70;

    do
    {
        delay(1);
        retval = _twoWire->writeTransmission(_hubAddress, _txBuffer, txLen);
    } while((retval != I2C_ERROR_OK) && (retries-- > 0));


    if(retval != I2C_ERROR_OK)
        return 0x71;

    delay(sleepMs);

    retries = DEFAULT_RETRIES;
    do
    {
        _twoWire->requestFrom(_hubAddress, static_cast<uint8_t>(1));
        statusByte = _twoWire->read();
        delay(sleepMs);
    } while((statusByte == static_cast<uint8_t>(SsStatus::ErrTryAgain)) &&
            (statusByte != static_cast<uint8_t>(SsStatus::Success)) && (retries-- > 0));

    return statusByte;
}

uint8_t MAX32664A::writeCmdWithData(uint8_t *cmdBytes, const uint8_t cmdBytesLen,
                                    uint8_t *data, const uint8_t dataLen,
                                    const int16_t delayMs)
{
    if(cmdBytesLen + dataLen > I2C_BUFFER_LENGTH)
        return 0x70;

    memcpy(_txBuffer, cmdBytes, cmdBytesLen);
    memcpy(_txBuffer + cmdBytesLen, data, dataLen);

    int status = writeCmd(cmdBytesLen + dataLen, delayMs);
    return status;
}

uint8_t MAX32664A::readCmd(uint8_t *cmdBytes, const int cmdLen,
                           uint8_t *data, const int dataLen,
                           uint8_t *rxBuffer, const uint8_t rxLen,
                           const int16_t delayMs)
{
    auto retries = DEFAULT_RETRIES;
    uint8_t retval;

    if(((cmdLen + dataLen) > I2C_BUFFER_LENGTH) || (rxLen > I2C_BUFFER_LENGTH))
        return 0x70;

    do
    {
        retval = _twoWire->writeTransmission(_hubAddress, cmdBytes, cmdLen, (dataLen == 0));
        if(dataLen != 0)
            retval |= _twoWire->writeTransmission(_hubAddress, data, dataLen);
        delay(1);
    } while((retval != I2C_ERROR_OK) && (retries-- > 0));

    if(retval != I2C_ERROR_OK)
        return retval;

    delay(delayMs);

    // We need a slighter bigger buffer.
    uint8_t rx[rxLen+1];

    retries = DEFAULT_RETRIES;
    do
    {
        retval = _twoWire->readTransmission(_hubAddress, rx, rxLen+1);
        delay(delayMs);
    } while((retval != I2C_ERROR_OK) &&
            (rx[0] != static_cast<uint8_t>(SsStatus::ErrTryAgain)) &&
            (retries-- > 0));

    // Copy back the result.
    memcpy(rxBuffer, &rx[1], rxLen);

    return rx[0];
}

/**
 * @brief MAX32664A::readSensorHubStatus
 * @param statusByte Byte status returned by the device.
 * @return The sensor hub status if statusByte == 0, undefined otherwise.
 */
uint8_t MAX32664A::readSensorHubStatus(uint8_t & statusByte)
{
    return readByte(FamilyByte::SensorHubStatus,
                    IndexByte::SensorHubStatus, statusByte);
}

/**
 * @brief MAX32664A::numSamplesOutFifo
 * in the FIFO.
 * @param statusByte 0 if the communication suceeded.
 * @return If statusByte == 0 returns the number of available samples, undefined
 * otherwise.
 */
uint8_t MAX32664A::numSamplesOutFifo(uint8_t & statusByte)
{
    return readByte(FamilyByte::ReadOutputFifo,
                    IndexByte::NumOfFifoSamples, statusByte);
}

/**
 * @brief MAX32664A::setOutputMode sets the output mode, ie, which data will be
 * presented in the reports in the FIFO.
 * @param mode
 * @return statusByte, 0 if successful.
 */
uint8_t MAX32664A::setOutputMode(const MAX32664A::OutputMode mode)
{
    return writeByte(FamilyByte::OutputMode, IndexByte::SetOutputFormat,
                     static_cast<uint8_t>(mode), DEFAULT_CMD_SLEEP_MS);
}

uint8_t MAX32664A::setFifoThreshold(const uint8_t threshold)
{
    return writeByte(FamilyByte::OutputMode, IndexByte::SetThreshold,
                     threshold, DEFAULT_CMD_SLEEP_MS);
}

uint8_t MAX32664A::agcAlgorithmControl(const Enable enable)
{
    return writeByte(FamilyByte::EnableAlgorithm, IndexByte::EnableAgcAlgo,
                     static_cast < uint8_t > (enable), DEFAULT_CMD_SLEEP_MS);
}

uint8_t MAX32664A::max30101Control(const MAX32664A::Enable enable)
{
    return writeByte(FamilyByte::EnableSensor, IndexByte::EnableMax30101,
                     static_cast < uint8_t > (enable), ENABLE_SENSOR_SLEEP_MS);
}

uint8_t MAX32664A::fastAlgorithmControl(const MAX32664A::AlgorithmMode mode)
{
    _algorithmMode = mode;
    return writeByte(FamilyByte::EnableAlgorithm, IndexByte::EnableWhrmAlgorithm,
                     static_cast < uint8_t > (mode), ENABLE_SENSOR_SLEEP_MS);
}
