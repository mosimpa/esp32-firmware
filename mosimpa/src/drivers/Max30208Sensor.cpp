#include "Max30208Sensor.h"

Max30208Sensor::Max30208Sensor(TwoWire &wirePort){
	_i2cPort = &wirePort;
}

/**
 * @brief Max30208Sensor::getTemperature
 * @return Temperature in ºC, floating point.
 *
 * The temperature is calculated as:
 *
 * \f$temp[ºC] = rawTemp[0.005 ºC] * 0.005\f$
 *
 * which is the same as:
 *
 * \f$temp[ºC] = rawTemp[0.005 ºC] / 200.0\f$
 */
float Max30208Sensor::getTemperature(void)
{
    return rawTemperature() / 200.0;
}

/**
 * @brief Max30208Sensor::getIntTemperature
 * @return temperature in 0.1ºC, integer.
 *
 * The temperature is calculated as:
 *
 * \f$temp[0.1ºC] = rawTemp[0.005 ºC] * 0.005 * 10\f$
 *
 * which is the same as:
 *
 * \f$temp[0.1ºC] = rawTemp[0.005 ºC] / 20\f$
 *
 * avoiding floating point conversions.
 *
 */
int16_t Max30208Sensor::getIntTemperature(void)
{
    return rawTemperature() / 20;
}

/**
 * @brief Max30208Sensor::rawTemperature
 * @return Temperature where each bit is 0.005 ºC.
 */
int16_t Max30208Sensor::rawTemperature()
{
    uint8_t readRaw[2] = {0};
    int16_t rawTemp = 0;

    // CONVERT_T
    I2CWriteByte(MAX30208_ADDRESS, MAX30208_SENSOR_SETUP, 0x01);

    delay(100);

    // TEMP_RDY == 1?
    if((I2CReadByte(MAX30208_ADDRESS, MAX30208_STATUS) & 0x01) == 0x01)
    {
            I2CReadBytes(MAX30208_ADDRESS, MAX30208_FIFO_DATA, &readRaw[0] ,2);
            rawTemp = readRaw[0] << 8 | readRaw[1];
    }

    return rawTemp;
}

void Max30208Sensor::begin(void){		

	I2CWriteByte(MAX30208_ADDRESS, MAX30208_SYSTEM_CONTROL, 0x01); 		// Reset
	I2CWriteByte(MAX30208_ADDRESS, MAX30208_GPIO_SETUP, 0x00); 			// GPIO0 and GPIO1: digital input (HiZ)

}

// _i2cPort->h read and write protocol
void Max30208Sensor::I2CWriteByte(uint8_t address, uint8_t subAddress, uint8_t data)
{
	_i2cPort->beginTransmission(address);  // Initialize the Tx buffer
	_i2cPort->write(subAddress);           // Put slave register address in Tx buffer
	_i2cPort->write(data);                 // Put data in Tx buffer
	_i2cPort->endTransmission();           // Send the Tx buffer
}

uint8_t Max30208Sensor::I2CReadByte(uint8_t address, uint8_t subAddress)
{
	uint8_t data; // `data` will store the register data
	_i2cPort->beginTransmission(address);
	_i2cPort->write(subAddress);
	_i2cPort->endTransmission(false);
	_i2cPort->requestFrom(address, (uint8_t) 1);
	data = _i2cPort->read();
	return data;
}

void Max30208Sensor::I2CReadBytes(uint8_t address, uint8_t subAddress, uint8_t * dest, uint8_t count)
{
	_i2cPort->beginTransmission(address);   // Initialize the Tx buffer
	_i2cPort->write(subAddress);
	_i2cPort->endTransmission(false);
	uint8_t i = 0;
	_i2cPort->requestFrom(address, count);  // Read bytes from slave register address
	while (_i2cPort->available())
	{
		dest[i++] = _i2cPort->read();
	}
}
