#include <Arduino.h>
#include <time.h>
#include "MqttClient.h"

#include "bloodpressuresensor.h"

BloodPressureSensor::BloodPressureSensor()
{
}

bool BloodPressureSensor::load(Data &data)
{
    if (count == BLOODP_SENSOR_BUFFER_SIZE) {
        return false;
    }
    buffer[indexWrite] = data;
    if (++indexWrite == BLOODP_SENSOR_BUFFER_SIZE) {
        indexWrite = 0;
    }
    count++;
    return true;
}

// {
//  "bloodP": [
//    { "time": 1212144844, "sys": 0, "dia" : 0 }
//  ]
// }
bool BloodPressureSensor::buildJson()
{
    StaticJsonDocument < DOC_JSON_SIZE > jsonDocument;
    Data data;
    String message;
    if (count == 0) {
        return false;
    }
    // Generacion JSON
    jsonDocument.clear();
    while (count > 0) {
        JsonArray arrayName = jsonDocument.createNestedArray("bloodP");
        read(&data);
        JsonObject arrayItemm = arrayName.createNestedObject();
        arrayItemm["time"] = data.time;
        arrayItemm["sys"] = data.sys;
        arrayItemm["dia"] = data.dia;
    }
    return true;
}

bool BloodPressureSensor::loadAndFlush(Data &data, MqttClient & mqttClient)
{
    load(data);
    flush(mqttClient);
}

void BloodPressureSensor::read(Data * data)
{
    if (count == 0) {
        return;
    }
    *data = buffer[indexRead];
    if (++indexRead == BLOODP_SENSOR_BUFFER_SIZE) {
        indexRead = 0;
    }
    count--;
}
