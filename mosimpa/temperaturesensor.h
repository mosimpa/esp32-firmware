#ifndef TEMPERATURESENSOR_H
#define TEMPERATURESENSOR_H

#define ARDUINOJSON_USE_LONG_LONG 1
#include <ArduinoJson.h>
#include "MqttClient.h"
#include <Wire.h>

#include "abstractsensor.h"
#include "src/drivers/Max30208Sensor.h"

class TemperatureSensor : public AbstractSensor
{
public:
    struct Data {
        time_t time;
        int16_t temp;
    };

    TemperatureSensor(TwoWire & wirePort = Wire);
    void begin();
    float getTemperature();
    int16_t getIntTemperature();
    bool load(TemperatureSensor::Data & data);
    bool buildJson();
    bool loadAndFlush(Data &data, MqttClient &mqttClient);

protected:
    void read(TemperatureSensor::Data * data);

    Max30208Sensor temperatureSensor;
    static const int TEMP_SENSOR_BUFFER_SIZE = 10;
    TemperatureSensor::Data buffer[TEMP_SENSOR_BUFFER_SIZE];
    uint8_t indexWrite;
    uint8_t indexRead;
    uint8_t count;
};


#endif // TEMPERATURESENSOR_H
