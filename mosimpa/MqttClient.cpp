/*
 * MoSimPa ESP32 firmware
 * Copyright 2020
 * Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 * Casanova Alejandro <acasanova.mails@gmail.com>
 * Pouso Marcelo Alejandro <mapouso86@gmail.com>
 */

#define ARDUINOJSON_USE_LONG_LONG 1
#include <ArduinoJson.h>

#include <ESPmDNS.h>
#include <string.h>
#include <stdio.h>
#include <sys/time.h>

#include "MqttClient.h"
#include "Configuration.h"
#include "printf.h"

#if USE_WIFI_SECURE == true
const char *ca_cert =
        "-----BEGIN CERTIFICATE-----\n"
        "MIIC3zCCAcegAwIBAgIUbfvC3lJ7PSs9pq3K+DDEIl8JPOMwDQYJKoZIhvcNAQEL\n"
        "BQAwFjEUMBIGA1UEAwwLcmFzcGJlcnJ5cGkwHhcNMjAwNDI4MTMxNDQ1WhcNMzAw\n"
        "NDI2MTMxNDQ1WjAWMRQwEgYDVQQDDAtyYXNwYmVycnlwaTCCASIwDQYJKoZIhvcN\n"
        "AQEBBQADggEPADCCAQoCggEBAKeqvaXrayQvzRiYfCofej7UboR8ED0qq6kpV6O+\n"
        "jnWZGJbogOcLPC0b7w1+RUni/jB8MNq2DLXKt/ZT2JxtMH08pmC1V8IctjOogOAu\n"
        "vdSrxvpHMzanUgMvU41ha5kpei2uh9i83c2lDlj/xRR0LAdN4XcJFsS2bpU2I+Zv\n"
        "mbCc4fkL8nLgBTaOldYw0WH5+uLJFpUgBtlvlhJaL0EKHc2+y5IpuxNsETf3LoUC\n"
        "a8JCzyx7uT8fd9ASexOFI6Tq/6ht4C/Z0KbKI6vwqPLiqm1D2RfoZDZlYU5KNZ6V\n"
        "e8jlsFQMTUWNCBbC77xEJJyGi56kfVn0l/XqqqVyOdI6f3MCAwEAAaMlMCMwCQYD\n"
        "VR0TBAIwADAWBgNVHREEDzANggtyYXNwYmVycnlwaTANBgkqhkiG9w0BAQsFAAOC\n"
        "AQEAVINnf0lAQ8UuXnCkm9xWCpnimNWIord61gJ7Cj1eU0FGqOAlDS/AtRagm0VE\n"
        "Az13O2n0/6Rhu7Afkgaf5cmpf0MEj9kayEhpstRE8F2nrGnqNRzqU1DWZsf1i0fI\n"
        "QL5UdTU7tdUfAm3rwDIWE0VcrEk0ucEv13+QQiWRrgxVTjrOIO5JhIua+q20aaCh\n"
        "dbTL4AuN+Gkn+3mlWzzOLNzt1Q40117uO00oJCtjGjkRoiku4u4K2ZeLxqlulJTT\n"
        "MTtypIhczh7pXN8FhGNQqvoHcx1YOwlLa/U34xwSdXxfZV8fkqebQ3TK1SLHhvWI\n"
        "vtYJW2fy/3/nDV6lLKTpWm49WA==\n" "-----END CERTIFICATE-----\n";
#endif

using namespace std::placeholders;

MqttClient::MqttClient():_pubSubClient(_mqttClient)
{
    callback = std::bind(&MqttClient::subscriptionCallback, this, _1, _2, _3);
    _pubSubClient.setCallback(callback);
    _lastNtpSync = 0;
}

bool MqttClient::connectWifi(const char *ssid, const char *pass)
{
    int status = WL_IDLE_STATUS;
    int attempts = MQTT_CONNECTION_ATTEMPTS;

    printf("Waiting for WiFi...");
    WiFi.begin(ssid, pass);
    while ((status != WL_CONNECTED) && (attempts > 0)) {
        status = WiFi.status();
        delay(500);
        Serial.print(".");
        attempts--;
    }

    if (status != WL_CONNECTED) {
        printfn("Couldn't get a wifi connection");
        return false;
    }
    // set SSL/TLS certificate
#if USE_WIFI_SECURE == true
    _mqttClient.setCACert(ca_cert);
    printfn("Connected Secure to wifi");
#else
    printfn("Connected to wifi");
#endif

    return true;
}

bool MqttClient::connectBroker(const char *hostname, int port)
{
    int attempts = MQTT_CONNECTION_ATTEMPTS;

    printfn("Starting MQTT connection...");

    IPAddress serverIp = MDNS.queryHost(hostname);

    if(serverIp != INADDR_NONE)
    {
        printfn("mDNS IP %s", serverIp.toString().c_str());
        _pubSubClient.setServer(serverIp, port);
    }
    else
    {
        printfn("mDNS resolution was unsuccessful, trying DNS resolution.");
        _pubSubClient.setServer(hostname, port);
    }

    // Attempting MQTT connection...
    while ((!_pubSubClient.connected()) && (attempts > 0))
    {
        if (_pubSubClient.connect(getID().c_str()))
        {
            printfn("connected...");
        } else {
            printfn("failed, rc = %u try again in 5 seconds",
                    _pubSubClient.state());
            delay(5000);
        }
        attempts--;
    }

    if (attempts <= 0)
        return false;

    return true;
}

bool MqttClient::isConnectedWiFi()
{
    if (WiFi.status() == WL_CONNECTED) {
        return true;
    }
    return false;
}

bool MqttClient::isConnectedBroker()
{
    return _pubSubClient.connected();
}

String MqttClient::getID()
{
    String id = WiFi.macAddress();
    id.replace(":", "");
    id.toLowerCase();

    return id;
}

bool MqttClient::publish(const char *topic, const char *message)
{
    _pubSubClient.publish(topic, message);
}

bool MqttClient::subscribeToHeartbeat()
{
    bool subscribed = _pubSubClient.subscribe(HEARTBEAT_TOPIC);

    if(subscribed)
        printfn("Subscribed to heartbeat");
    else
        printfn("Not subscribed to heartbeat");

    return subscribed;
}

void MqttClient::loop()
{
    _pubSubClient.loop();
}

bool MqttClient::getEpoch(time_t &time)
{
    struct timeval tv;
    auto retval = gettimeofday(&tv, NULL);

    if(retval < 0)
        return false;

    time = tv.tv_sec;
    return true;
}

void MqttClient::subscriptionCallback(const char * topic, byte* payload, unsigned int length)
{
    if(strncmp(HEARTBEAT_TOPIC, topic, strlen(HEARTBEAT_TOPIC)) != 0)
        return;

    byte pLoad[length + 1];
    memcpy(pLoad, payload, length);
    pLoad[length] = '\0';

    StaticJsonDocument<64> doc;
    DeserializationError error = deserializeJson(doc, pLoad);

    if (error)
    {
        printf("deserializeJson() failed: ");
        printfn("%s", error.c_str());
        return;
    }

    _lastNtpSync = doc["time"].as<time_t>();
    struct timeval tv;
    tv.tv_sec = _lastNtpSync;
    settimeofday(&tv, NULL);

    if(_pubSubClient.unsubscribe(HEARTBEAT_TOPIC))
        printfn("Unsubscribed form heartbeat");
    else
        printfn("Could not unsubscribe from heartbeat");
}
