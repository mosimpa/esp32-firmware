/*
 * MoSimPa ESP32 firmware
 * Copyright 2020
 * Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 * Casanova Alejandro <acasanova.mails@gmail.com>
 * Pouso Marcelo Alejandro <mapouso86@gmail.com>
 */

#ifndef MQTT_CLIENT_H
#define MQTT_CLIENT_H
#include "Configuration.h"
#include <inttypes.h>
#include <functional>
#include <Arduino.h>
#include <WiFi.h>
#include <sys/time.h>
#include "src/WiFiClientSecure/src/WiFiClientSecure.h"
#include "src/PubSubClient/src/PubSubClient.h"

class MqttClient {

public:
    MqttClient();
    bool connectWifi(const char *ssid, const char *pass);
    bool connectBroker(const char *ip, int port);
    bool isConnectedWiFi();
    bool isConnectedBroker();
    String getID();
    bool publish(const char *topic, const char *message);
    bool subscribeToHeartbeat();

    void loop();

    bool getEpoch(time_t & time);
    time_t lastNtpSync() { return _lastNtpSync; }

private:
    static constexpr char* HEARTBEAT_TOPIC = "datakeeper/heartbeat";
    static constexpr int MQTT_CONNECTION_ATTEMPTS = 5;
    void subscriptionCallback(const char * topic, byte* payload, unsigned int length);
    MQTT_CALLBACK_SIGNATURE;
    time_t _lastNtpSync;

#if USE_WIFI_SECURE == true
    WiFiClientSecure _mqttClient;
#else
    WiFiClient _mqttClient;
#endif
    PubSubClient _pubSubClient;
};
#endif
