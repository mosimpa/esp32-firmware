/*
 * MoSimPa ESP32 firmware
 * Copyright 2020
 * Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 * Casanova Alejandro <acasanova.mails@gmail.com>
 * Pouso Marcelo Alejandro <mapouso86@gmail.com>
 */
#ifndef HEARTRATESENSOR_H
#define HEARTRATESENSOR_H

#define ARDUINOJSON_USE_LONG_LONG 1
#include <ArduinoJson.h>
#include "MqttClient.h"

#include "abstractsensor.h"

class HeartRateSensor : public AbstractSensor
{
public:

    struct Data {
        time_t time;
        uint16_t heartR;
    };

    HeartRateSensor();

    bool load(Data & data);
    bool buildJson();
    bool loadAndFlush(Data & data, MqttClient &mqttClient);

protected:
    void read(Data * data);

    static const int HEARTR_SENSOR_BUFFER_SIZE = 10;
    Data buffer[HEARTR_SENSOR_BUFFER_SIZE];
};

#endif // HEARTRATESENSOR_H
