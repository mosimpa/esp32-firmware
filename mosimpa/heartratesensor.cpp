#include <Arduino.h>
#include <time.h>
#include "MqttClient.h"

#include "heartratesensor.h"

HeartRateSensor::HeartRateSensor()
{
}

bool HeartRateSensor::load(Data &data)
{
    if (count == HEARTR_SENSOR_BUFFER_SIZE) {
        return false;
    }
    buffer[indexWrite] = data;
    if (++indexWrite == HEARTR_SENSOR_BUFFER_SIZE) {
        indexWrite = 0;
    }
    count++;
    return true;
}

// {
//  "heartR": [
//    { "time": 1212144844, "heartR": 0}
//  ]
// }
bool HeartRateSensor::buildJson()
{
    Data data;
    if (count == 0) {
        return false;
    }
    // Generacion JSON
    jsonDocument.clear();
    while (count > 0) {
        JsonArray arrayName = jsonDocument.createNestedArray("heartR");
        read(&data);
        JsonObject arrayItemm = arrayName.createNestedObject();
        arrayItemm["time"] = data.time;
        arrayItemm["heartR"] = data.heartR;
    }
    return true;
}

bool HeartRateSensor::loadAndFlush(Data & data, MqttClient & mqttClient)
{
    load(data);
    flush(mqttClient);
}

void HeartRateSensor::read(Data *data)
{
    if (count == 0) {
        return;
    }
    *data = buffer[indexRead];
    if (++indexRead == HEARTR_SENSOR_BUFFER_SIZE) {
        indexRead = 0;
    }
    count--;
}
