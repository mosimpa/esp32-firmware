#ifndef ABSTRACTSENSOR_H
#define ABSTRACTSENSOR_H

#define ARDUINOJSON_USE_LONG_LONG 1
#include <ArduinoJson.h>
#include "MqttClient.h"

class AbstractSensor
{
public:
    /// Size of the static JSON document to be generated.
    static const size_t DOC_JSON_SIZE = JSON_ARRAY_SIZE(1) +
                                        JSON_OBJECT_SIZE(1) +
                                        JSON_OBJECT_SIZE(3);

    AbstractSensor();

    void clear();
    virtual bool buildJson() = 0;
    void flush(MqttClient & mqttClient);
    StaticJsonDocument < DOC_JSON_SIZE > getJson();

protected:
    uint8_t indexWrite;
    uint8_t indexRead;
    uint8_t count;
    StaticJsonDocument < DOC_JSON_SIZE > jsonDocument;
};

#endif // ABSTRACTSENSOR_H
