/*
 * MoSimPa ESP32 firmware
 * Copyright 2020
 * Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 * Casanova Alejandro <acasanova.mails@gmail.com>
 * Pouso Marcelo Alejandro <mapouso86@gmail.com>
 */

#ifndef CONFIGURATION_H
#define CONFIGURATION_H
#include <inttypes.h>
#include <Arduino.h>

#define FIRMWARE_VERSION    "0.0.4"
#define HARDWARE_VERSION    "0.0.1"

#define USE_WIFI_SECURE     FALSE

#define INPUT_WAIT_TIME_MS 10000

#define SERIAL_BAUDRATE     115200
#define LED_BUILTIN1        5
#define LED_BUILTIN2        17
#define LED_BUILTIN3        16
#define BATTERY_VOLTAGE_GPIO 35 ///< ADC1 CH7
#define I2C_FRECUENCY_HZ    100000
#define I2C_SDA_SENSOR_HUB  33
#define I2C_SCL_SENSOR_HUB  32
#define SENSOR_HUB_RESTN    27
#define SENSOR_HUB_MFIO     14
#define I2C_SDA_TEMPERATURE   18
#define I2C_SCL_TEMPERATURE   19
#define TEMPERATURE_POWER_SOURCE_ENABLE 21
#define STATUS_FINGER_DETECT  3
#define MIN_DATA_CONFIDENCE   95
#define SENSOR_HUB_WAIT_DATA_MAX  10
#define SENSOR_CONFIG_ERROR_COUNT  10

#define DELAY_INIT_SETUP_MS 100
#define DELAY_CONFIGURATION_STATUS_MS 100
#define DELAY_READ_SENSORS_MS 2000
#define DELAY_SENSOR_CONFIG_MS 1000
#define BLINK_LED_PERIOD  0.25

#define PUBLISH_THRESHOLD_MIN_MS 6000
#define PUBLISH_THRESHOLD_MAX_MS 10000

/// Period in ms in which to report device status.
#define PERIOD_PUBLISH_INFO_DEVICE_SEC 60

/// COEF A = 1.5958422*100000
#define SENSOR_HUB_COEF_A 0x00026F60
/// COEF B = -34.6596622*100000
#define SENSOR_HUB_COEF_B 0xFFCB1D12
/// COEF C = 112.6898759*100000
#define SENSOR_HUB_COEF_C 0x00ABF37B

/// 25 bytes (per standard) + null character
#define WIFI_SSID_SIZE 26
/// 25 bytes + null character
#define WIFI_PASS_SIZE 26

/**
 * Buffer to hold the MQTT broker URL. It needs to be big enough to be used
 * in various cases. See https://gitlab.com/mosimpa/esp32-firmware/-/issues/20
 */
#define MQTT_BROKER_HOSTNAME_SIZE 64

/// NTP sync period: each hour.
#define NTP_SYNC_PERIOD_MS 3600000UL

typedef struct {
    char flag;
    char wifissid[WIFI_SSID_SIZE];
    char wifipass[WIFI_PASS_SIZE];
    char mqtthostname[MQTT_BROKER_HOSTNAME_SIZE];
    int mqttport;
} Parameters;

class ConfigurationClass {
protected:
    Parameters parameters;
public:
    ConfigurationClass();
    void begin(void);
    bool isSetting(void);
    char *getSSID(void);
    char *getPass(void);
    char *getMqttHostname(void);
    int getMqttPort(void);
    void setSSID(String ssid);
    void setPass(String pass);
    void setMqttPort(int port);
    void setMqttHostname(String hostname);
    void commit(void);
};

extern ConfigurationClass Configuration;

#endif
