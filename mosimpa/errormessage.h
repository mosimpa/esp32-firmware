#ifndef ERRORMESSAGE_H
#define ERRORMESSAGE_H

#include <limits>
#include <WString.h>

class ErrorMessage
{
public:
    ErrorMessage(const String file, const String func, const int line,  const int retval = std::numeric_limits<int>::min());

    String json() const;

    inline bool operator==(const ErrorMessage& rhs)
    {
        if((this->_file == rhs._file) &&
           (this->_func == rhs._func) &&
           (this->_line == rhs._line) &&
           (this->_retval == rhs._retval))
            return true;

        return false;
    }

private:
    String _file;
    String _func;
    int _line;
    int _retval;
};

#endif // ERRORMESSAGE_H
