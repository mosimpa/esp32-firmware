#ifndef ERRORMESSAGEQUEUE_H
#define ERRORMESSAGEQUEUE_H

#include <deque>

#include "errormessage.h"

class ErrorMessageQueue
{
public:
    ErrorMessageQueue();

    void add(const ErrorMessage & msg);
    bool isEmpty() { return _q.empty(); }
    ErrorMessage takeFirst();

private:
    static const int MAX_NUM_MSG = 3;
    std::deque<ErrorMessage> _q;
};

#endif // ERRORMESSAGEQUEUE_H
