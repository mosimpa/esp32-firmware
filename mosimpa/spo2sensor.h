/*
 * MoSimPa ESP32 firmware
 * Copyright 2020
 * Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 * Casanova Alejandro <acasanova.mails@gmail.com>
 * Pouso Marcelo Alejandro <mapouso86@gmail.com>
 */
#ifndef SPO2SENSOR_H
#define SPO2SENSOR_H

#define ARDUINOJSON_USE_LONG_LONG 1
#include <ArduinoJson.h>
#include "MqttClient.h"

#include "abstractsensor.h"

class SpO2Sensor : public AbstractSensor
{
public:
    struct Data {
        time_t time;
        uint16_t spO2;
    };

    SpO2Sensor();
    bool load(SpO2Sensor::Data & data);
    bool buildJson();
    bool loadAndFlush(SpO2Sensor::Data & data, MqttClient &mqttClient);

protected:
    void read(SpO2Sensor::Data * data);

    static const uint8_t SPO2_SENSOR_BUFFER_SIZE  = 10;
    SpO2Sensor::Data buffer[SPO2_SENSOR_BUFFER_SIZE];
};

#endif // SPO2SENSOR_H
