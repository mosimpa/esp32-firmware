/*
 * MoSimPa ESP32 firmware
 * Copyright 2020
 * Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 * Casanova Alejandro <acasanova.mails@gmail.com>
 * Pouso Marcelo Alejandro <mapouso86@gmail.com>
 */

/**
 * \todo We are sending the password in clear.
 */

#include <string.h>

#define ARDUINOJSON_USE_LONG_LONG 1
#include <ArduinoJson.h>
#include <ArduinoJson/version.hpp>
#include <Wire.h>
#include <Ticker.h>
#include <ESPmDNS.h>
#include <WiFi.h>

#include <core_version.h>

#include "printf.h"
#include "Configuration.h"
#include "ConfigurationWebServer.h"
#include "MqttClient.h"
#include "max32664ahub.h"
#include "abstractsensor.h"
#include "temperaturesensor.h"
#include "errormessagequeue.h"

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

#ifndef ARDUINO_ESP32_RELEASE_1_0_5
#error This code was tested with version 1.0.5 of the Arduino ESP32 SDK.
#endif

#if (ARDUINOJSON_VERSION_MAJOR != 6) || (ARDUINOJSON_VERSION_MINOR != 18) || (ARDUINOJSON_VERSION_REVISION != 0)
#error This code was tested with version 6.18.0 of the ArduinoJson library.
#endif

/* ------------------------------------------------------------- */
ErrorMessageQueue errorMessageQueue;
/* ------------------------------------------------------------- */
TwoWire temperatureSensorI2C = TwoWire(0);
TwoWire oxygenAndHeartRateSensorI2C = TwoWire(1);
bioData nextReportBioData;

/* ------------------------------------------------------------- */
ConfigurationWebServer webServer;	///< web server utilizado para realizar la configuracion del dispositivo (ssid, pass, mqtt ip, mqtt port)
MqttClient mqttClient;		///< Cliente MQTT que envia los datos al broker
bool isDeviceConfigured = false;	///< El dispositivo esta configurado? No, iniciamos AP
Ticker tickerLed;		///< Configuramos un ticker periodico para Led
Ticker tickerPublishDeviceInfo;	///< Configuramos un ticker periodico para envio de datos de info del dispositivo (ID, Bateria, time)
StaticJsonDocument < 200 > infoJson;	///< Json usado para el envio de info del dispositivo

TemperatureSensor temperatureSensor(temperatureSensorI2C);
MAX32664AHub spo2AndHeartRateHub(SENSOR_HUB_RESTN, SENSOR_HUB_MFIO,
                                      oxygenAndHeartRateSensorI2C);

uint32_t mqttMessageId = 0;
uint8_t emptyHubDataCount = 0;

enum Leds
{
    LED_ALIVE = LED_BUILTIN1,
    LED_NETWORK = LED_BUILTIN2,
    LED_SENSORS = LED_BUILTIN3
};

/* ------------------------------------------------------------- */
bool mqttClientInitialize(void);
bool mqttClientConnectionStatus(void);
void publishInfoDevice(void);

void readSensorHub(void);
void readSensorTemperature(void);
void publishSensorsData(void);

/* ------------------------------------------------------------- */
void blinkLed1()
{
    digitalWrite(LED_BUILTIN1, !digitalRead(LED_BUILTIN1));
}

/**
 * @brief setup Initialization.
 */
void setup()
{
    Serial.begin(SERIAL_BAUDRATE);
    printfn("-- MoSimPa VERSION - v%s\r\n", FIRMWARE_VERSION);

    nextReportBioData = {};

    Configuration.begin();
    isDeviceConfigured = Configuration.isSetting();

    configureLeds();
    waitForSerialCommand();
    configureWifi();
    configureSensors();

    /*
     * Note form Lisandro: I sincerely don't know why this delay is here, but
     * if it's not here the connection sometimes crashes the CPU...
     */
    delay(500);
}

/**
 * @brief loop Main loop.
 */
void loop()
{
    static unsigned long int sensorStart = millis();
    static unsigned long int timeSyncStart = sensorStart;

    if (isDeviceConfigured == false)
    {
        // Procesamiento del servidor web.
        // Responde a las peticiones del cliente y recibe la configuracion
        if (webServer.handleClient() == true)
            isDeviceConfigured = mqttClientInitialize();

        delay(DELAY_CONFIGURATION_STATUS_MS);
    }
    else
    {
        if(digitalRead(SENSOR_HUB_MFIO) == LOW)
            readSensorHub();

        // Controla conexion con el broker.. Si no esta conectado reintenta conexión.
        if(!mqttClientConnectionStatus())
            return;

        if((mqttClient.lastNtpSync() == 0) ||
           (millis() - timeSyncStart > NTP_SYNC_PERIOD_MS))
        {
            mqttClient.subscribeToHeartbeat();
            timeSyncStart = millis();
        }

        // At this point we are connected to WiFi and the broker.
        // Let the MQTT client do it's job.
        mqttClient.loop();

        // Send error messages if we have them.
        while(!errorMessageQueue.isEmpty())
        {
            publishErrorMessage(errorMessageQueue.takeFirst());
        }

        // Do not send data if we do not have a valid time reference.
        if(mqttClient.lastNtpSync() == 0)
            return;

        /*
         * Send data when:
         * - PUBLISH_THRESHOLD_MAX_MS have passed by.
         * - PUBLISH_THRESHOLD_MIN_MS and we have a minimum of confidence in
         * the HR measurement.
         */
        if(((millis() - sensorStart > PUBLISH_THRESHOLD_MAX_MS)) ||
           (((millis() - sensorStart) > PUBLISH_THRESHOLD_MIN_MS) &&
            (nextReportBioData.confidence >= MIN_DATA_CONFIDENCE) &&
            (nextReportBioData.status == 3)))
        {
            loadSpo2AndHeartRateData(nextReportBioData);
            nextReportBioData = {};

            readSensorTemperature();

            publishSensorsData();
            sensorStart = millis();
        }
    }
}

/**
 * @brief ledOn Configure and initialize LEDs
 * @param turnOn
 * @param led
 */
void ledOn(bool turnOn, Leds led)
{
    if (turnOn) {
        digitalWrite(led, LOW);
    } else {
        digitalWrite(led, HIGH);
    }
}

void configureLeds()
{
    // led alive
    pinMode(LED_BUILTIN1, OUTPUT);
    ledOn(true, LED_ALIVE);
    tickerLed.attach(BLINK_LED_PERIOD, blinkLed1);

    // led network status
    pinMode(LED_BUILTIN2, OUTPUT);
    ledOn(false, LED_NETWORK);

    // led sensors status
    pinMode(LED_BUILTIN3, OUTPUT);
    ledOn(false, LED_SENSORS);
}

/**
 * @brief configureWifi Configure and initialize WiFI
 */
void configureWifi()
{
    if (isDeviceConfigured == true)
    {
        // Se conectara a wifi y al broker configurado
        printfn("EEPROM configured:");
        printfn("\tssid = %s", Configuration.getSSID());
        printfn("\tpass = %s", Configuration.getPass());
        printfn("\tmqtthostname = %s", Configuration.getMqttHostname());
        printfn("\tmqttport = %u", Configuration.getMqttPort());
        printfn("MAC address: %s", WiFi.macAddress().c_str());

        if (mqttClientInitialize())
        {
            publishInfoDevice();
            tickerPublishDeviceInfo.attach(PERIOD_PUBLISH_INFO_DEVICE_SEC,
                                           publishInfoDevice);
        }
    }
    else
    {
        // Inicia modo AP y habilita servidor web
        printfn("EEPROM not configured");
        webServer.begin();
    }
}

/**
 * @brief configureSensors Sensors configuration.
 */
void configureSensors()
{
    printfn("Start sensors configuration...");

    configureSensorTemperature();
    configureSensorHub();

    printfn("Sensor configuration Done!");
}

void publishErrorMessage(const ErrorMessage & msg)
{
    String topic("errors/" + mqttClient.getID());
    mqttClient.publish(topic.c_str(), msg.json().c_str());
    printfn("\r\n- Published error message.");
}

/**
 * @brief configureSensorTemperature()
 * Configure the sensor temperature driver in order to use it.
 *
 * We also enable it's power source and keep it on. The sensor should use 0.5µA
 * while in standby, and it gets into standby automatically as soon as it finishes
 * doing a temperature read.
 */
void configureSensorTemperature()
{
    // Turn on the power.
    pinMode(TEMPERATURE_POWER_SOURCE_ENABLE, OUTPUT);
    digitalWrite(TEMPERATURE_POWER_SOURCE_ENABLE, HIGH);
    delay(35);

    temperatureSensorI2C.begin(I2C_SDA_TEMPERATURE, I2C_SCL_TEMPERATURE,
                               I2C_FRECUENCY_HZ);
    temperatureSensor.begin();
}

/**
 * @brief configureSensorHub configures the bio hub.
 */
void configureSensorHub()
{
    int status = -1;
    int error_count = 0;
    int32_t coefficients[3] = { 0, 0, 0 };

    oxygenAndHeartRateSensorI2C.begin(I2C_SDA_SENSOR_HUB,
                                      I2C_SCL_SENSOR_HUB,
                                      I2C_FRECUENCY_HZ);

    while (status != 0)
    {
        status = spo2AndHeartRateHub.begin();

        switch (status) {
        case 0x0:
            printfn("Sensor Hub started!");
            break;

        case 0x8:
            printfn("Device is in bootloader mode, something went wrong!!!");
            break;

        case 0xFF:
            printfn("Could not communicate with the sensor hub: %d", status);
            break;
        }

        if (status != 0)
        {
            errorMessageQueue.add(ErrorMessage(__FILENAME__, __func__, __LINE__, status));

            continue;
        }

        printfn("Reading default calibration coefficients...");
        spo2AndHeartRateHub.readCoefficients(coefficients);
        printfn("\tCoef A: 0x%x", coefficients[0]);
        printfn("\tCoef B: 0x%x", coefficients[1]);
        printfn("\tCoef C: 0x%x", coefficients[2]);

        status = spo2AndHeartRateHub.configAlgorithm(SENSOR_HUB_COEF_A,
                                                    SENSOR_HUB_COEF_B,
                                                    SENSOR_HUB_COEF_C);
        if (status == 0)
            printfn("Sensor hub configured.");
        else
        {
            printfn("Error configuring sensor hub: %d", status);
            errorMessageQueue.add(ErrorMessage(__FILENAME__, __func__, __LINE__, status));
        }

        error_count++;
        if (error_count >= SENSOR_CONFIG_ERROR_COUNT)
        {
            ledOn(true, LED_SENSORS);
            printfn("*** Sensor ERROR!");
            break;
        }
        delay(DELAY_SENSOR_CONFIG_MS);
    }

    if (status == 0)
    {
        // Read sensor hub coefficients
        printfn("Reading calibration coefficients...");
        spo2AndHeartRateHub.readCoefficients(coefficients);
        printfn("\tCoef A: 0x%x, we sent 0x%x", coefficients[0], SENSOR_HUB_COEF_A);
        printfn("\tCoef B: 0x%x, we sent 0x%x", coefficients[1], SENSOR_HUB_COEF_B);
        printfn("\tCoef C: 0x%x, we sent 0x%x", coefficients[2], SENSOR_HUB_COEF_C);

        printfn("Init sensors interruptions and process...");
    }
}

/**
 * @brief waitForSerialCommand Waits for serial command to set the device in
 * Server mode(AP) in order to be configured or MQTT Client (normal state).
 */
void waitForSerialCommand()
{
    Serial.println("Select mode: [s] = Server; [c] = Client");

    unsigned long startMs = millis();

    while (true) {
        if (Serial.available() > 0) {
            char cmd = Serial.read();
            Serial.print("I received: ");
            Serial.println(cmd);
            if (cmd == 's') {
                isDeviceConfigured = false;
            }
            break;
        }

        if ((millis() - startMs) >= INPUT_WAIT_TIME_MS) {
            printfn("Timeout, will try client mode.");
            break;
        }
    }
}

/********************************************************************************
 * Inicia el cliente MQTT. Se conecta al wifi y al broker
 ********************************************************************************/
/**
 * @brief mqttClientInitialize Connects the device to WiFi and starts the MQTT
 * client.
 * @return true if sucessful.
 */
bool mqttClientInitialize(void)
{
    String id = mqttClient.getID().c_str();
    printfn("MQTT client ID: %s", id.c_str());

    if(!mqttClient.connectWifi(Configuration.getSSID(), Configuration.getPass()))
        return false;

    if (!MDNS.begin(String("mosimpa-device-" + id).c_str()))
    {
        Serial.println("Error setting up mDNS responder!");
        return false;
    }

    return mqttClient.connectBroker(Configuration.getMqttHostname(), Configuration.getMqttPort());
}

/**
 * @brief mqttClientConnectionStatus Checks the connection against the MQTT
 * broker. It will re try the connection if it's not up.
 */
bool mqttClientConnectionStatus(void)
{
    if (!mqttClient.isConnectedBroker())
    {
        ledOn(true, LED_NETWORK);
        if(mqttClient.isConnectedWiFi())
        {
            mqttClient.connectBroker(Configuration.getMqttHostname(),
                                     Configuration.getMqttPort());
            return false;
        }
        else
        {
            mqttClientInitialize();
            return false;
        }
    }

    ledOn(false, LED_NETWORK);

    return true;
}

/**
 * @brief battmV
 * @return The battery Voltage in milli Volts.
 *
 * Currently the PCB has a divider of 100kOhm + 10kOhm. Defining:
 * - \f$V_{Bat}\f$ battery voltage
 * - \f$V_{Div}\f$ voltage in the divider
 *
 * Then:
 *
 * \f$V_{Bat}=11*V_{Div}\f$
 *
 * The ADC is <a href="https://github.com/espressif/esp-idf/issues/164">non linear</a>
 * but in error in the region of operation seems beareable.
 */
uint16_t battmV()
{
    static const int NUM_OF_SAMPLES = 5;
    uint32_t sum = 0;

    for(int i = 0; i < NUM_OF_SAMPLES; i++)
        sum += 11 * analogRead(BATTERY_VOLTAGE_GPIO);

    return static_cast<uint16_t>(sum / NUM_OF_SAMPLES);
}

/**
 * @brief publishInfoDevice Publish data to the MQTT broker.
 */
void publishInfoDevice(void)
{
    String message;
    infoJson.clear();
    time_t epoch;

    if(!mqttClient.getEpoch(epoch))
        return;

    if (mqttClient.isConnectedBroker()) {
        infoJson["WiFiMAC"] = mqttClient.getID();
        infoJson["hw_ver"] = HARDWARE_VERSION;
        infoJson["firm_ver"] = FIRMWARE_VERSION;
        infoJson["battmV"] = battmV();
        infoJson["time"] = epoch;
        infoJson["msgId"] = (uint32_t) mqttMessageId++;
        serializeJson(infoJson, message);

        printfn("%s", message.c_str());
        mqttClient.publish("devices/info", message.c_str());
    }
}

/**
 * @brief readSensorHub
 *
 * Keeps trying to get a reasonable sample for 10 seconds at most.
 */
void readSensorHub()
{
    const unsigned long start = millis();
    bioData bioDataArray[MAX32664AHub::MAX_REPORTS_NUMBER];
    uint8_t retval;
    uint8_t numReportsRead;
    uint8_t totalNumReports;
    bool read = false;

    retval = spo2AndHeartRateHub.readHrSpO2(bioDataArray, MAX32664AHub::MAX_REPORTS_NUMBER,
                                            numReportsRead, totalNumReports);
    if(retval != 0)
    {
        errorMessageQueue.add(ErrorMessage(__FILENAME__, __func__, __LINE__, retval));
        return;
    }

    printfn("BioHub: retrieved %d out of %d reports in the FIFO.", numReportsRead, totalNumReports);

    for(int i = 0; i < numReportsRead; i++)
    {
        if((bioDataArray[i].status == STATUS_FINGER_DETECT) &&
                (bioDataArray[i].confidence > nextReportBioData.confidence))
            nextReportBioData = bioDataArray[i];
    }
}

void readSensorTemperature()
{
    int16_t temp = temperatureSensor.getIntTemperature();
    loadTemperatureData(temp);
}

void loadSpo2AndHeartRateData(bioData sensorData)
{
    SpO2Sensor::Data oxygenData;
    HeartRateSensor::Data heartRateData;
    time_t epoch;

    if(!mqttClient.getEpoch(epoch))
        return;

    oxygenData.time = epoch;
    oxygenData.spO2 = sensorData.oxygen;
    heartRateData.time = epoch;
    heartRateData.heartR = sensorData.heartRate;
    printfn("\r\n- Sensor Hub Data:");
    printfn("\tConfidence: %d", sensorData.confidence);
    printfn("\tOxygen: %d", oxygenData.spO2);
    printfn("\tHeart Rate: %d", heartRateData.heartR);
    spo2AndHeartRateHub.clear();
    spo2AndHeartRateHub.load(oxygenData, heartRateData);
}

void loadTemperatureData(int16_t temperature)
{
    TemperatureSensor::Data temperatureData;
    time_t epoch;

    if(!mqttClient.getEpoch(epoch))
        return;

    temperatureData.time = epoch;
    temperatureData.temp = temperature;
    printfn("\r\n- Temperature: %d", temperatureData.temp);
    temperatureSensor.clear();
    temperatureSensor.load(temperatureData);
}

void mergeJsons(JsonObject dest, JsonObjectConst src)
{
    for (auto kvp:src) {
        dest[kvp.key()] = kvp.value();
    }
}

void publishSensorsData()
{
    StaticJsonDocument<3 * AbstractSensor::DOC_JSON_SIZE> jsonDoc;
    String message;

    if (mqttClient.isConnectedBroker())
    {
        // Create an empty doc.
        jsonDoc.to<JsonObject>();

        if(temperatureSensor.buildJson())
        {
            mergeJsons(jsonDoc.as<JsonObject>(),
                       temperatureSensor.getJson().as<JsonObject>());
        }

        if (spo2AndHeartRateHub.buildJson())
        {
            mergeJsons(jsonDoc.as<JsonObject>(),
                       spo2AndHeartRateHub.getHeartRateJson().as<JsonObject>());

            mergeJsons(jsonDoc.as<JsonObject>(),
                       spo2AndHeartRateHub.getSpo2Json().as<JsonObject>());
        }

        serializeJson(jsonDoc, message);
        String topic("reads/" + mqttClient.getID());
        mqttClient.publish(topic.c_str(), message.c_str());
        printfn("\r\n- Published sensors' data!");
    }
}
