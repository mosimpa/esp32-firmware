#include <string.h>
#include "errormessagequeue.h"

ErrorMessageQueue::ErrorMessageQueue()
{
}

void ErrorMessageQueue::add(const ErrorMessage &msg)
{
    // Do not store copies.
    for(int i=0; i < _q.size(); i++)
        if(_q.at(i) == msg)
            return;

    // Not a copy, ensure that we have space for it.
    while(_q.size() >= MAX_NUM_MSG)
    {
        _q.pop_front();
    }

    // Finally add the message at the end of the queue.
    _q.push_back(msg);
}

ErrorMessage ErrorMessageQueue::takeFirst()
{
    if(_q.empty())
       return ErrorMessage(__FILE__, __func__, __LINE__, 0);

    ErrorMessage e = _q.front();
    _q.pop_front();

    return e;
}
