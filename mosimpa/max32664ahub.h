/*
 * MoSimPa ESP32 firmware
 * Copyright 2020
 * Lisandro Damián Nicanor Pérez Meyer <perezmeyer@gmail.com>
 * Casanova Alejandro <acasanova.mails@gmail.com>
 * Pouso Marcelo Alejandro <mapouso86@gmail.com>
 */

#ifndef MAX32664A_HUB_H
#define MAX32664A_HUB_H
#include <inttypes.h>
#include <Arduino.h>
#define ARDUINOJSON_USE_LONG_LONG 1
#include <ArduinoJson.h>
#include "time.h"
#include <Wire.h>
#include "src/drivers/max32664a.h"
#include "spo2sensor.h"
#include "heartratesensor.h"

class MAX32664AHub {
public:
    static const int MAX_REPORTS_NUMBER = MAX32664A::MAX_REPORTS_NUMBER;

    MAX32664AHub(uint16_t rstn, uint16_t mfio, TwoWire & wirePort);
    uint8_t begin();
    uint8_t
    configAlgorithm(const int32_t coefA,
                    const int32_t coefB,
                    const int32_t coefC);
    uint8_t readHrSpO2(bioData * bData, const uint8_t size, uint8_t & numReportsRead, uint8_t &totalNumReports);
    uint8_t readCoefficients(int32_t coefArr[3]);
    void clear();
    bool load(SpO2Sensor::Data &spo2Data, HeartRateSensor::Data & hrData);
    bool buildJson();
    void flush(MqttClient & mqttClient);
    bool loadAndFlush(SpO2Sensor::Data &spo2Data,
                      HeartRateSensor::Data & hrData,
                      MqttClient &mqttClient);
    StaticJsonDocument < AbstractSensor::DOC_JSON_SIZE > getSpo2Json();
    StaticJsonDocument < AbstractSensor::DOC_JSON_SIZE > getHeartRateJson();

protected:
    MAX32664A bioHub;
    SpO2Sensor spo2Sensor;
    HeartRateSensor heartRateSensor;
};

#endif // MAX32664A_HUB_H
