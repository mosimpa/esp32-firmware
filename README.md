# MoSimPa's ESP32 firmware

This repository contains [MoSimPa](http://mosimpa.gitlab.io/)'s ESP32 firmware
code.

It makes use of [GIT
flow](https://nvie.com/posts/a-successful-git-branching-model/), so remember:

- master is the production branch.
- develop is the actual development branch from which you should start working
  from.

Happy hacking!

# Connecting a programmer to the ESP32

| UART | ESP32 |
|------|-------|
| RX   | TX    |
| TX   | RX    |
| GND  | GND   |
| RTS  | EN    |
| DTR  | GPIO0 |
