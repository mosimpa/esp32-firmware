/*
 * I2C scanner i2c address
*/

#include "printf.h"
#include <Ticker.h>
#include <Wire.h>
#include "I2CScanner.h"

/* ------------------------------------------------------------- */
#define TEST_MAX32664_I2C   1

#define LED_BUILTIN         16

#define I2C_SDA_MAX32664    33
#define I2C_SCL_MAX32664    32

#define I2C_SDA_MAX30208    18
#define I2C_SCL_MAX30208    19

#define I2C_FRECUENCY_HZ    100000

/* ------------------------------------------------------------- */
Ticker tickerLed;
TwoWire I2CTest = TwoWire(0);
I2CScanner scanner(I2CTest);

/* ------------------------------------------------------------- */
void blinkLed(){
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
}

/* ------------------------------------------------------------- */
/**
 * \brief Inicializacion
 */
void setup() {  

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  tickerLed.attach(0.25, blinkLed);
  Serial.begin(115200);
  Serial.println("Scan I2C Address Started:");

#if TEST_MAX32664_I2C
  I2CTest.begin(I2C_SDA_MAX32664, I2C_SCL_MAX32664, I2C_FRECUENCY_HZ);
#else
  I2CTest.begin(I2C_SDA_MAX30208, I2C_SCL_MAX30208, I2C_FRECUENCY_HZ);
#endif
  scanner.Init();
}

/**
 * \brief bucle infinito
 */
void loop(){
  scanner.Scan();
  delay(5000);
}

