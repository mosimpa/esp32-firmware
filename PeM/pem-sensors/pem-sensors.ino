/*
 * MoSimPa ESP32 Sensors PeM
*/

#include "printf.h"
#include <Ticker.h>
#include <Wire.h>
#include "Max30208Sensor.h"
#include "Max32664.h"

/* ------------------------------------------------------------- */
#define FIRMWARE_VERSION    "2.3"

#define LED_BUILTIN1        5
#define LED_BUILTIN2        17
#define LED_BUILTIN3        16

#define I2C_SDA_MAX32664    33
#define I2C_SCL_MAX32664    32
#define MAX3264_RESTN       27
#define MAX3264_MFIO        14

#define I2C_SDA_MAX30208    18
#define I2C_SCL_MAX30208    19

#define STATUS_FINGER_DETECT  3
#define MIN_DATA_CONFIDENCE   95

/* ------------------------------------------------------------- */

Ticker tickerLed;
TwoWire I2CMax30208 = TwoWire(0);
TwoWire I2CMax32664 = TwoWire(1);

Max30208Sensor tempSensor(I2CMax30208);
Bio_Sensor_Hub bioHub(MAX3264_RESTN, MAX3264_MFIO);

bioData bodyData;
float bodyTemperature;
float spo2 = 0.0;
float heartRate = 0.0;
int32_t sampleNum = 0;
int32_t coefArr[3];
volatile bool max32664kbhit = false;

/* ------------------------------------------------------------- */

void blinkLed(){
  digitalWrite(LED_BUILTIN1, !digitalRead(LED_BUILTIN1));
}

void mfio_detect() {
  digitalWrite(LED_BUILTIN2, !digitalRead(LED_BUILTIN2));
  max32664kbhit = true;
}

/* ------------------------------------------------------------- */
/**
 * Inicializacion
 */
void setup() {  
  // Inicializacion Leds
  pinMode(LED_BUILTIN1, OUTPUT);
  digitalWrite(LED_BUILTIN1, LOW);
  pinMode(LED_BUILTIN2, OUTPUT);
  digitalWrite(LED_BUILTIN2, LOW);
  pinMode(LED_BUILTIN3, OUTPUT);
  digitalWrite(LED_BUILTIN3, LOW);
  tickerLed.attach(0.25, blinkLed);
  
  Serial.begin(115200);
  printfn("-- PEM-Sensors - v%s\r\n", FIRMWARE_VERSION);

  // Inicializacion Max30208
  I2CMax30208.begin(I2C_SDA_MAX30208, I2C_SCL_MAX30208, 100000);
  tempSensor.begin();

 // Inicializacion Max32664
  I2CMax32664.begin(I2C_SDA_MAX32664, I2C_SCL_MAX32664, 100000);
  int result = bioHub.begin(I2CMax32664);
  if (result == 0) 
    printfn("Sensor started!");
  else
    printfn("Could not communicate with the sensor!!!");


  printfn("Reading calibration coefficients....");
  bioHub.readMaximFastCoef(coefArr);
  printfn("Coef A: %x\r\n", coefArr[0]);
  printfn("Coef B: %x\r\n", coefArr[1]);
  printfn("Coef C: %x\r\n", coefArr[2]);

  printfn("New calibration coefficients....");
  coefArr[0] = 1.5958422*100000;
  coefArr[1] = -34.6596622*100000;
  coefArr[2] = 112.6898759*100000;
  printfn("New Coef A: %x\r\n", coefArr[0]);
  printfn("New Coef B: %x\r\n", coefArr[1]);
  printfn("New Coef C: %x\r\n", coefArr[2]);

  Serial.println("Configuring Sensor...."); 
  int error = bioHub.configBpm(MODE_ONE, coefArr[0], coefArr[1], coefArr[2], false); 
  if(error == 0){ 
    Serial.println("Sensor configured.");
  }
  else {
    printfn("Error configuring sensor.");
    printfn("Error: %d", error);  
  }

  printfn("Reading new calibration coefficients....");
  coefArr[0] = 0;
  coefArr[1] = 0;
  coefArr[2] = 0;
  bioHub.readMaximFastCoef(coefArr);
  printfn("New Coef A: %x\r\n", coefArr[0]);
  printfn("New Coef B: %x\r\n", coefArr[1]);
  printfn("New Coef C: %x\r\n", coefArr[2]);  

  printfn("Loading up the buffer with data....");
  delay(4000);
  attachInterrupt(digitalPinToInterrupt(MAX3264_MFIO), mfio_detect, RISING);
}

/**
 * bucle infinito
 */
void loop(){
  
  bodyData = bioHub.readBpm();
  if (bodyData.status == STATUS_FINGER_DETECT && bodyData.confidence >= MIN_DATA_CONFIDENCE) {
      heartRate = bodyData.heartRate/10.0;
      spo2 = bodyData.oxygen/10.0;
      printfn("- Valid Data: %d\r\n", sampleNum++);
      printfn("\tConfidence: %d\r\n", bodyData.confidence);
      printfn("\tHeartrate: %.1f\r\n", heartRate);
      printfn("\tOxygen: %.1f\r\n", spo2);

      bodyTemperature = tempSensor.getTemperature();
      printfn("- Body Temperature: %f\r\n", bodyTemperature);
    }
  
#if 0
  delay(250);
#else
  while(max32664kbhit == false);
#endif
}

