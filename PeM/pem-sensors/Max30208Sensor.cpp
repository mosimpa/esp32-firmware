#include "Max30208Sensor.h"

Max30208Sensor::Max30208Sensor(TwoWire &wirePort){
	_i2cPort = &wirePort;
}

float Max30208Sensor::getTemperature(void){
	uint8_t readRaw[2] = {0};
	float temperature = -1000;

	I2CWriteByte(MAX30208_ADDRESS, MAX30208_SENSOR_SETUP, 0x01); 		// CONVERT_T
	delay(100);
	if((I2CReadByte(MAX30208_ADDRESS, MAX30208_STATUS) & 0x01) == 0x01){ // TEMP_RDY == 1?
		I2CReadBytes(MAX30208_ADDRESS, MAX30208_FIFO_DATA, &readRaw[0] ,2); // read two bytes
		int16_t raw = readRaw[0] << 8 | readRaw[1];  //combine two bytes
		temperature = raw  * 0.005;     // convert to temperature
	}
	return  temperature;
}

void Max30208Sensor::begin(void){		

	I2CWriteByte(MAX30208_ADDRESS, MAX30208_SYSTEM_CONTROL, 0x01); 		// Reset
	I2CWriteByte(MAX30208_ADDRESS, MAX30208_GPIO_SETUP, 0x00); 			// GPIO0 and GPIO1: digital input (HiZ)

}

// _i2cPort->h read and write protocol
void Max30208Sensor::I2CWriteByte(uint8_t address, uint8_t subAddress, uint8_t data)
{
	_i2cPort->beginTransmission(address);  // Initialize the Tx buffer
	_i2cPort->write(subAddress);           // Put slave register address in Tx buffer
	_i2cPort->write(data);                 // Put data in Tx buffer
	_i2cPort->endTransmission();           // Send the Tx buffer
}

uint8_t Max30208Sensor::I2CReadByte(uint8_t address, uint8_t subAddress)
{
	uint8_t data; // `data` will store the register data
	_i2cPort->beginTransmission(address);
	_i2cPort->write(subAddress);
	_i2cPort->endTransmission(false);
	_i2cPort->requestFrom(address, (uint8_t) 1);
	data = _i2cPort->read();
	return data;
}

void Max30208Sensor::I2CReadBytes(uint8_t address, uint8_t subAddress, uint8_t * dest, uint8_t count)
{
	_i2cPort->beginTransmission(address);   // Initialize the Tx buffer
	_i2cPort->write(subAddress);
	_i2cPort->endTransmission(false);
	uint8_t i = 0;
	_i2cPort->requestFrom(address, count);  // Read bytes from slave register address
	while (_i2cPort->available())
	{
		dest[i++] = _i2cPort->read();
	}
}
